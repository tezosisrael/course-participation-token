const Token = artifacts.require("Token");

module.exports = async (deployer, _network, accounts) => {
  const owner = accounts[0];
  const totalSupply = 1000;

  deployer.deploy(Token, {
    owner,
    totalSupply,
    ledger: map(owner, totalSupply),
    metadata: map(0, {
      tokenId: 0,
      symbol: "MFIL",
      name: "Tezos Israel and Madfish Solutions Workshop Certificate",
      description:
        "This certificate verifies that the holder of its private key attended, contributed and completed the Tezos Israel and Madfish Solution Workshop on December 7th to the 9th, 2020.  The certificate holder utilized skills in smart contract development and tokenization to build, test and deploy a token on the Tezos blockchain.",
      decimals: 0,
      extras: map({ imageUrl: "https://" }),
    }),
  });
};
