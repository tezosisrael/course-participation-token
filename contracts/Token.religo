#include "types.religo"
#include "transfer.religo"
#include "balanceof.religo"



type entryPoint =
| Transfer(list (transfer))
| Balance_of(balanceOfParam)
| Update_operators(list(updateOperator))
| Token_metadata_registry(contract(address))

type returnType = (list (operation), storage);

let main = ((action, storage): (action, storage)): returnType => 
  switch(action) {
    | Transfer(transfers) => transfer(transfers, storage)
    | Balance_of(params) => balanceOf(params, storage)
    | Update_operators(params) => failwith("FA2_OPERATORS_UNSUPPORTED")
    | Token_metadata_registry(callback) => tokenMetadataRegistry(callback, storage)
  }