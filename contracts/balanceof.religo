type balanceOfRequest = {
  owner: address;
  token_id: tokenId;
}

type balance_of_response = {
  request: balanceOfRequest;
  balance: nat;
}

type balanceOfParam = {
  requests: list(balanceOfRequest),
  callback: contract(list(balance_of_response));
}


/* 
  balance_of




errors:
FA2_TOKEN_UNDEFINED = if token id is not defined
 */


// !balance_of