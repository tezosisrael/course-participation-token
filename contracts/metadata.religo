

/*
  metadata:

  type token_id = nat



@view token_metadata_registry -> returns self


 */

let tokenMetadataRegistry = (callback: contract(address), storage:  storage): returnType => {
  let transaction = Tezos.transaction(Tezos.self_address, 0, callback)
  return ([transaction]: list(operation), storage)
}

// !metadata