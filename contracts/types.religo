type tokenId = nat;

type tokenMetadata = {
  token_id: tokenId,
  symbol: string,
  name: string,
  decimals: nat,
  extras: map(string, string)
}

type storage = {
  admin: address,
  ledger: big_map(account, nat),
  totalSupply: nat,
  token_metadata: big_map(tokenId, tokenMetadata)
};


type operatorParam = {
  owner: address,
  operator: address,
  token_id: tokenId,
}

type update_operator =
  | Add_operator(operatorParam)
  | Remove_operator(operatorParam);