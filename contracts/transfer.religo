
type transferDestination = {
  to_: address,
  token_id: tokenId,
  amount: nat,
}

type transfer = {
  from_: address,
  txs: list(transferDestination)
}


/*
transfer:

FA2_INSUFFICIENT_BALANCE - error for insufficient funds in one source account
FA2_TX_DENIED - error for not allowed to transfer
*/


let isTransferDesitnationAllowed = (isAllowedTansfer, tx) : (bool, transferDestination) => {
  isAllowedTansfer && tx.token_id == 0n && tx.amount == 1;
  // TODO should we check for double transfers? as we should allow only one token per user
  // and by specs transfer should cause source - tx.amount and target + tx.amount
}

let isTransferAllowed = ((isAllowed, transfer) : (bool, transfer)) : bool => {
  if (!isAllowed || tranfer.from_ != storage.admin) {
    false
  } else {
    List.fold (isTransferDesitnationAllowed, transfer.txs, true)
  }
}

let isAllowed = (transfers: list(transfer)) : bool => {
  if (Tezos.sender != storage.admin) {
    false
  } else {
    List.fold (isTransferAllowed, transfers, true);
  }
}

let transformTransferList = (transfers: list(transfer)) : list(address) {
  List.fold(((l, transfer): (list(address), transfer)) => {
      List.fold((l, tx): (list(address), transferDestination) => {
        return []
      }, transfer.txs, []: list(address))

    }, transfers,  []: list(address))
}

let updateLedger = (transfers: list(address), ledger: map(address, bool)) : ledger = {
  let updateLedgerRec = (transfers: list(address), )
}


let tranfer = (transfers: list(transfer), storage : storage): returnType => {
  if (!isAllowed(transfers)) {
    failwith("FA2_TX_DENIED")
  } else {

    let transferList = 

    let storage = {
      ...storage,
      ledger: ledger
    };

    (nil : list(operation), storage) : returnType;
  }
}